
## introduction

a chatbot with react as the frontend and flask as the backend

## how to use it?  

step1:
```sh
cd backend-flask and install backend
```

step2:
```sh
cd frontend-react-app and install frontend
```

step3:   
start to use the mini-chat system

input your question and click "answer question" button

